var cityData = require("../data/city.list.min.json");

module.exports = {
  filterCities: function(searchTerm) {
    return cityData.filter(function(city) {
      return city.name.startsWith(searchTerm);
    });
  },
};
