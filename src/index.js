const express = require("express");
const search = require("./search");
const app = express();
const port = 3005;

//Enable CORS so we can get the list of cities from our Frontend
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", (req, res) => {
  if (req.query.search !== null) {
    var cities = search.filterCities(req.query.search);
    if (cities.length > 0) {
      res.send(JSON.stringify(cities));
    } else {
      res.send("No cities found for that search term");
    }
  }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
