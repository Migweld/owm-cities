const functions = require("firebase-functions");
const cors = require('cors')({
  origin: true,
});
const search = require("./search");

exports.searchCities = functions.https.onRequest((req, res) => {
  if (req.query.search !== null) {
    var cities = search.filterCities(req.query.search);
    return cors(req, res, () => {
    if (cities.length > 0) {
      res.send(JSON.stringify(cities));
    } else {
      res.send("No cities found for that search term");
    }
  });
  }
});
