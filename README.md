## OpenWeatherMap Cities

A small Node application to search and return an array of cities based on a search term.

A demo version is available at https://us-central1-owm-cities.cloudfunctions.net/searchCities?search=Vancouver

### How to use

- Clone repo
- `cd <repo directory>`
- `npm (or yarn) install`
- `node src/index.js`
- Send queries to `localhost:3005?search=<term>`
